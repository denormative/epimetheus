#!/usr/bin/env python -u

import os
import sys
from epi.config import baseTemplateDir, Info, info
from epi.loadnovel import loadNovel
from epi.createpdf import createPDF
from epi.createepub import createEPUB
from re import compile
from epi.filehandling import grabPage
import urllib2

#TODO: os.makedirs() in function that is about to start loading files

availableTemplates = []

def loadAvailableTemplates():
	global availableTemplates
	availableTemplates = [ f for f in os.listdir(baseTemplateDir)
						  if os.path.isfile(os.path.join(baseTemplateDir, f)) ]

def listAll():
	count = 0
	for f in availableTemplates:
		count +=1
		print "	%3d) %s" % (count, f)

def buildAll(buildPDF, buildEPUB, buildIOS, reloadPageCache):
	global info

	for f in availableTemplates:
		info.reset()
		buildOne(f, buildPDF, buildEPUB, buildIOS, reloadPageCache)


def buildOne(templateName, buildPDF, buildEPUB, buildIOS, reloadPageCache):
	# FIXME: handle isURL case?
	# if it's a url, add the 'novel volume name' part to the list, and then at least it'll
	# download into the cache I think.

	# strip off the local directory path information if for some reason we're provided it.
	#templateName = templateName.rpartition('\\')[2]
	templateName = templateName.rpartition(os.sep)[2]

	for t in availableTemplates:
		if t==templateName:
			loadNovel(templateName, reloadPageCache)
			#createPDF(templateName, "Nothing")
			if buildPDF:
				createPDF(templateName, "Page")

			if buildEPUB:
				createEPUB(templateName, buildIOS)

			return

	sys.exit("Could not find novel template: " + templateName)

def downloadTemplate(url, reloadPageCache):
	reURL = compile("^http://(?:www.)+baka-tsuki.org/project/index.php\?title=(.*)$")

	print url
	if(reURL.match(url)):
		title = reURL.match(url).group(1)

		title = urllib2.unquote(title).decode('utf8')
		print title
		grabPage(baseTemplateDir, title, reloadPageCache)


if __name__=='__main__':
	if(len(sys.argv)>=2):
		#loadAvailableTemplates()

		doDownloadTemplate = False
		doListAll = False
		doBuildAll = False
		buildOneFilename = ""
		buildPDF = False
		buildEPUB = False
		buildIOS = False
		reloadPageCache = False

		for i in sys.argv[1:]:
			if(i.startswith("http://")):
				doDownloadTemplate = True
				buildOneFilename = i
			elif(i=="--list-all"): doListAll = True
			elif(i=="--build-all"): doBuildAll = True
			elif(i=="--pdf"): buildPDF = True
			elif(i=="--epub"): buildEPUB = True
			elif(i=="--ios"): buildIOS = True
			elif(i=="--reload-page-cache"): reloadPageCache = True
			else:
				#buildOneFilename = i
				print "%r" % i
				availableTemplates.append(os.path.basename(i))

		if(buildPDF == False and buildEPUB == False and
			doListAll == False and doDownloadTemplate == False):
			# when in doubt, build a pdf.
			buildPDF = True

		if doDownloadTemplate:
			downloadTemplate(buildOneFilename, reloadPageCache)
		elif doListAll:
			loadAvailableTemplates()
			listAll()
		elif doBuildAll:
			loadAvailableTemplates()
			buildAll(buildPDF, buildEPUB, buildIOS, reloadPageCache)
		elif len(availableTemplates)>=1:
			buildAll(buildPDF, buildEPUB, buildIOS, reloadPageCache)
		#else: buildOne(buildOneFilename, buildPDF, buildEPUB, buildIOS, reloadPageCache)
	else:
		sys.exit(
			"Usage:\n"
			"\t" + sys.argv[0] + " --list-all\n"
			"\t" + sys.argv[0] + " [options] --build-all\n"
			"\t" + sys.argv[0] + " [options] <novel-template>\n"
			"\t" + sys.argv[0] + " [options] <http://baka-tsuki.org/project/index.php?title=novel-template>\n"
			"Options:\n"
			"\t--epub\toutput .epub files\n"
			"\t--pdf\toutput .pdf files\n"
			"\t--ios\toutput .epub files styled for ios with <aside type='footnote'> rather then reference pages\n"
			"\t--reload-page-cache reload the page (but not image) cache\n"
			#"\t--\t\n"
			#"\t--\t\n"
			#"\t--\t\n"
		)
