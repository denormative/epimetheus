#!/usr/bin/env python

baseCacheDir = "cache/"

baseTemplateDir = "templates/"

basePDFOutputDir = ""
baseEPUBOutputDir = ""

class Info:
    # set this to 'chapter' or 'root'
    referencePlacement = "chapter"
    # set this to 'novel' or 'magazine'
    headingStyle = 'novel'
    highlightColor = '#365f91'

    title = "undefined"
    filename = None
    story = None
    illustrator = None
    translator = None
    editor = None
    pdfMaker = None

    def __init__(self):
        pass
    
    def reset(self):
        self.referencePlacement = "chapter"
        self.headingStyle = "novel"
        self.highlightColor = '#365f91'
        
        self.title = "undefined"
        self.filename = None
        self.story = None
        self.illustrator = None
        self.translator = None
        self.editor = None
        self.pdfMaker = None

info = Info()
