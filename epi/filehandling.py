#!/usr/bin/env python

import mwclient
import sys
import os

site = None

def sanitiseFileName(fileName):
    return fileName.replace(":", "").replace("*", "").replace("?", "").replace('"', "").replace("<", "").replace(">", "").replace("|", "")
    #.replace("\\", "").replace("/", "")

def sanitiseFileNameCompletely(fileName):
    return fileName.replace(":", "").replace("*", "").replace("?", "").replace('"', "").replace("<", "").replace(">", "").replace("|", "").replace("\\", "").replace("/", "")

def grabImage(localFileDir, fileName):
    global site

    localFileName = (localFileDir + "/").decode('utf8')

    # stripping out any characters crappy-NTFS doesn't handle...
    localFileName += fileName.replace("\\", "").replace("/", "").replace(":", "").replace("*", "").replace("?", "").replace('"', "").replace("<", "").replace(">", "").replace("|", "")

    print "%r" % localFileName
    print "%r" % fileName
    if(os.path.isfile(localFileName)):
        print "Image: '" + fileName + "' cached."
        return localFileName

    try:
        if site is None:
            site = mwclient.Site('www.baka-tsuki.org', path="/project/")

        image = site.Images[fileName]
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        sys.exit("Something went wrong trying to get "+fileName+". Exiting.")
    except:
        print "Unexpected error:", sys.exc_info()[0]
        sys.exit("Something went wrong trying to get "+fileName+". Exiting.")

    fr = image.download()
    fw = open(localFileName, 'wb')

    print "Downloading: '" + fileName + "'",

    try:
        while True:
            s = fr.read(4096)
            if not s: break
            fw.write(s)
            print ".",
        print ""
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

        fw.close()
        os.unlink(localFileName)

        fr.close()
        sys.exit("Something went wrong trying to get "+fileName+". Exiting.")

    fr.close()
    fw.close()

    return localFileName

def grabPage(localFileDir, fileName, reloadPageCache):
    global site

    localFileName = (localFileDir + "/").decode('utf8')

    # stripping out any characters crappy-NTFS doesn't handle...
    localFileName += fileName.replace("\\", "").replace("/", "").replace(":", "").replace("*", "").replace("?", "").replace('"', "").replace("<", "").replace(">", "").replace("|", "")

    if(reloadPageCache == False and os.path.isfile(localFileName)):
        print "Page: '" + fileName + "' cached."
        return localFileName

    page = None

    try:
        if site is None:
            site = mwclient.Site('www.baka-tsuki.org', path="/project/")

        page = site.Pages[fileName]
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        sys.exit("Something went wrong trying to get "+fileName+". Exiting.")

    fw = open(localFileName, 'wb')

    print "Downloading: '" + fileName + "'"
    # metadata: page.touched page.revision

    try:
        fw.write(page.edit().encode('utf-8'))
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

        fw.close()
        os.unlink(localFileName)

        fr.close()
        sys.exit("Something went wrong trying to get "+fileName+". Exiting.")

    fw.close()

    return localFileName

