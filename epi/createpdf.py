#!/usr/bin/env python

import datetime
import sys

from re import compile, sub, IGNORECASE, UNICODE
from config import info, basePDFOutputDir, baseCacheDir
from particles import Particle, particleList
from filehandling import sanitiseFileName, grabImage

from reportlab.platypus import *
from reportlab.platypus.flowables import HRFlowable

from reportlab.pdfgen import canvas

from reportlab.lib import *
from reportlab.lib.units import *
from reportlab.lib.styles import ParagraphStyle, ListStyle
from reportlab.lib.enums import *
from reportlab.lib.colors import Color

elements = []

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFontFamily

pdfmetrics.registerFont(TTFont('ArialUnicode', 'Arial Unicode MS.ttf'))
registerFontFamily('ArialUnicode',normal='ArialUnicode',bold='ArialUnicode',italic='ArialUnicode',boldItalic='ArialUnicode')
_baseFontName = 'ArialUnicode'
_baseFontNameB = 'ArialUnicode'
_baseFontNameI = 'ArialUnicode'
_baseFontNameBI = 'AriaUnicode'

#pdfmetrics.registerFont(TTFont('Arial', 'arial.ttf'))
#pdfmetrics.registerFont(TTFont('ArialBd', 'arialbd.ttf'))
#pdfmetrics.registerFont(TTFont('ArialIt', 'ariali.ttf'))
#pdfmetrics.registerFont(TTFont('ArialBI', 'arialbi.ttf'))
#registerFontFamily('Arial',normal='Arial',bold='ArialBd',italic='ArialIt',boldItalic='ArialBI')
#_baseFontName = 'Arial'
#_baseFontNameB = 'ArialBd'
#_baseFontNameI = 'ArialI'
#_baseFontNameBI = 'AriaBl'

spaceBeforeMod = 0.8
leadingMod = 1.2

PStyle = ParagraphStyle(name='Normal',
	fontName = _baseFontName,
	fontSize=15.5,
	spaceBefore=spaceBeforeMod*15.5,
	leading=leadingMod*15.5,
	allowWidows=0,
	allowOrphans=0,
	)

ListPStyle = ParagraphStyle(name='Normal',
	fontName = _baseFontName,
	fontSize=15.5,
	spaceBefore=spaceBeforeMod*8,
	leading=leadingMod*15.5,
	allowWidows=0,
	allowOrphans=0,
	)

CenterPStyle = ParagraphStyle(name='Center Normal',
	fontName = _baseFontName,
	fontSize=15.5,
	spaceBefore=spaceBeforeMod*15.5,
	leading=leadingMod*15.5,
	allowWidows=0,
	allowOrphans=0,
	alignment=TA_CENTER,
	)

Heading2Style = ParagraphStyleHeading2Style = ParagraphStyle(name='Heading2',
	fontName = _baseFontName,
	fontSize=22,
	spaceBefore=spaceBeforeMod*22,
	leading=leadingMod*22,
	textColor=info.highlightColor
	)

ReferencesHeading2Style = ParagraphStyle(name='ReferencesHeading2',
	fontName = _baseFontName,
	fontSize=22,
	spaceBefore=spaceBeforeMod*22,
	leading=leadingMod*22,
	textColor=info.highlightColor
	)

Heading3Style = ParagraphStyle(name='Heading3',
	fontName = _baseFontNameB,
	fontSize=17,
	spaceBefore=spaceBeforeMod*17,
	leading=leadingMod*17,
	textColor=info.highlightColor,
	alignment=TA_CENTER,
	)

Heading4Style = ParagraphStyle(name='Heading4',
	fontName = _baseFontNameB,
	fontSize=15,
	spaceBefore=spaceBeforeMod*15,
	leading=leadingMod*15,
	textColor=info.highlightColor,
	)

LStyle = ListStyle(name='List',
	bulletFontName = _baseFontName,
	bulletFontSize=15.5,
	bulletOffsetY=-0.1*15.5,
	leading=leadingMod*8,
	bulletFormat=lambda s: s+".",
	leftIndent=27,
	)

DefinitionListStyle = ListStyle(name='DefinitionList',
	bulletFontName = _baseFontName,
	bulletFontSize=15.5,
	bulletOffsetY=-0.1*15.5,
	leading=leadingMod*8,
	bulletFormat=lambda s: "",
	leftIndent=27,
	)

def updateHighlightColor():
	Heading2Style.textColor = info.highlightColor
	Heading3Style.textColor = info.highlightColor
	Heading4Style.textColor = info.highlightColor

class LNDocTemplate(BaseDocTemplate):
	_invalidInitArgs = ('pageTemplates',)
	_normalFrame = None

	firstPageWasImage = None

	def __init__(self, filename, **kw):
		BaseDocTemplate.__init__(self, filename, **kw)

		frameN = Frame(self.leftMargin, self.bottomMargin, self.width, self.height, id='normal')
		self._normalFrame = frameN
		frameI = Frame(0, 0, self.pagesize[0], self.pagesize[1], 0, 0, 0, 0, id='image')

		self.addPageTemplates([
						PageTemplate(id='Normal',frames=frameN, pagesize=self.pagesize),
						PageTemplate(id='Image', frames=frameI, pagesize=self.pagesize)])

	lastOutlineDepth = 0
	outlineUid = 0
	def addOutlineEntry(self, text, depth):
		# adjust depths so we can't get errors leaping around erratically the outline tree
		if(depth-self.lastOutlineDepth>1):
			depth = self.lastOutlineDepth + 1
		self.lastOutlineDepth = depth

		self.outlineUid+=1
		self.chapter = text
		key = 'ch%d' % self.outlineUid
		self.canv.bookmarkPage(key)
		self.canv.addOutlineEntry(text, key, depth, 0)

	def afterFlowable(self, flowable):
		"Registers TOC entries."
		if flowable.__class__.__name__ == 'Paragraph':
			#text = flowable.getPlainText()
			style = flowable.style.name
			if style == 'Heading2':
				#self.notify('TOCEntry', (0, flowable.getPlainText(), self.page))
				self.addOutlineEntry(flowable.getPlainText(), 1)
			elif style == 'DisclaimerHeaderStyle': # we're special...
				if(flowable.getPlainText()=="Disclaimer"):
					#self.notify('TOCEntry', (0, flowable.getPlainText(), self.page))
					self.addOutlineEntry("Disclaimer/Credits", 1)
			elif style == 'ReferencesHeading2': # we're special too!
				#self.notify('TOCEntry', (0, flowable.getPlainText(), self.page))
				self.addOutlineEntry(flowable.getPlainText(), 2)
			elif style == 'Heading3':
				#self.notify('TOCEntry', (1, flowable.getPlainText(), self.page))
				self.addOutlineEntry(flowable.getPlainText(), 2)
			elif style == 'Heading4':
				#self.notify('TOCEntry', (2, flowable.getPlainText(), self.page))
				self.addOutlineEntry(flowable.getPlainText(), 3)

	def beforeDocument(self):
		self.addOutlineEntry(self.title, 0)

	def handle_pageBegin(self):
		'''override base method to add a change of page template after the firstpage.
		'''
		self._handle_pageBegin()

	def handle_documentBegin(self):
		self._handle_documentBegin()

		if self.firstPageWasImage is not None:
			for t in self.pageTemplates:
				if t.id == self.firstPageWasImage:
					self.pageTemplate = self.pageTemplates[self.pageTemplates.index(t)]
					return
			raise ValueError, "can't find template('%s')"%pt

	def build(self,flowables,canvasmaker=canvas.Canvas):
		"""build the document using the flowables.  Annotate the first page using the onFirstPage
			   function and later pages using the onLaterPages function.  The onXXX pages should follow
			   the signature

					def myOnFirstPage(canvas, document):
						# do annotations and modify the document
						...

			   The functions can do things like draw logos, page numbers,
			   footers, etcetera. They can use external variables to vary
			   the look (for example providing page numbering or section names).
		"""
		self._calc()	#in case we changed margins sizes etc
		BaseDocTemplate.build(self,flowables, canvasmaker=canvasmaker)

def newPage(modifiedCurrentPage):
	if modifiedCurrentPage:
		elements.append(PageBreak())
		modifiedCurrentPage = False

	return modifiedCurrentPage

def addSpacer(width, height):
	elements.append(Spacer(width, height))

# ignore the ASCII-ish characters, and also the en/em-dashes and horizontal elipsis
reNonEnglishChars = compile(u"([^\u0000-\u00FF\u2013\u2014\u2026]+)")
def fixUnicode(txt):
	txt = sub(reNonEnglishChars, lambda match: "<font name='ArialUnicode'>%s</font>" % match.group(1), txt)
	txt = txt.replace(u"\u3000", u"&nbsp;&nbsp;")

	return txt

def temp(match):
	print "fnord %r" % "&amp;%s" % match.group(1)
	return "&amp;%s" % match.group(1)

#reFixBrokenAmp    = compile("&([^#][^0-9]|[^a-z][^a-z])", IGNORECASE)
reFixBrokenAmp1    = compile("&([^#a-z][^a-z])", IGNORECASE)
def buildParagraph(txt):
	global alterParaPre
	global alterParaPost
	global alterParaPop

	for x in xrange(len(alterParaPre)-1, -1, -1):
		pre = alterParaPre[x]
		for s in pre:
			txt = s + txt
		post = alterParaPost[x]
		for s in post:
			txt = txt + s

	for x in xrange(alterParaPop):
		alterParaPre.pop();
		alterParaPost.pop();
	alterParaPop = 0

	print "%r" % txt

	return txt

def addParagraph(txt, style):
	global alterParaPre
	global alterParaPost
	global alterParaPop

	#txt = sub(reFixBrokenAmp, lambda match: "&amp;%s" % match.group(1), txt)
	txt = sub(reFixBrokenAmp1, temp, txt)

	if alterParaPop > len(alterParaPre):
		sys.exit("Huh?")
	if len(alterParaPre) > 0:
		txt = buildParagraph(txt)
		elements.append(Paragraph(txt, style))
	else:
		elements.append(Paragraph(txt, style))

def addHR():
	elements.append(HRFlowable(width='100%',thickness=1.5,color=Color(.5, .5, .5, 1),lineCap='square'))

DisclaimerHeaderStyle = ParagraphStyle(name='DisclaimerHeaderStyle',
	fontName = _baseFontName,
	fontSize=30.75,
	leading=41,
	alignment=TA_CENTER
	)

DisclaimerTextStyle = ParagraphStyle(name='DisclaimerTextStyle',
	fontName = _baseFontName,
	fontSize=20.75,
	leading=23
	)

def appendDisclaimer(doc, modifiedCurrentPage):
	modifiedCurrentPage = newPage(modifiedCurrentPage)

	addHR()
	addSpacer(0, 0.25*cm)
	addParagraph("Disclaimer", DisclaimerHeaderStyle)
	addSpacer(0, 0.5*cm)

	addParagraph("Under no circumstances would you be allowed to take this \
work for commercial activities or for personal gain. Baka-Tsuki does not and will not condone any activities of such, \
including but not limited to rent, sell, print, auction.", DisclaimerTextStyle)

	addSpacer(0, 1*cm)

	addHR()
	addSpacer(0, 0.25*cm)
	addParagraph("Credits", DisclaimerHeaderStyle)
	addSpacer(0, 0.5*cm)

	credits = []
	if info.story:
		credits.append(("Story", ":", Paragraph(info.story, DisclaimerTextStyle)))
	if info.illustrator:
		credits.append(("Illustrator", ":", Paragraph(info.illustrator, DisclaimerTextStyle)))
	if info.translator:
		credits.append(("Translator", ":", Paragraph(info.translator, DisclaimerTextStyle)))
	if info.editor:
		credits.append(("Editor", ":", Paragraph(info.editor, DisclaimerTextStyle)))
	if info.pdfMaker:
		credits.append(("PDF Maker", ":", Paragraph(info.pdfMaker, DisclaimerTextStyle)))

	tableStyle = TableStyle(['FONT', (0,0), (1,-1), _baseFontName, 18, 19])
	if len(credits)>0:
		elements.append(Table(credits, colWidths=[0.2*doc.width, 0.05*doc.width, 0.75*doc.width],
							style=[
								('FONT', (0,0), (-1,-1), _baseFontName, 20.75, 21),
								('VALIGN', (0,0), (-1,-1), 'TOP')
							]))

	addSpacer(0, 1*cm)
	addHR()

	addParagraph("Generated on %s" % datetime.datetime.now().ctime(), CenterPStyle)

	return True

def insertImage(cacheDir, imageName, scaleImagesTo, doc, modifiedCurrentPage, unmodifiedFirstPage):
	print ">> %r:%r" % (cacheDir, imageName)
	localFileName = grabImage(cacheDir, imageName)

	im = Image(localFileName)

	#print "%r %r %r %r %r %r %r" % (localFileName, im.imageWidth, im.imageHeight, doc.pagesize[0], doc.pagesize[1], doc.width, doc.height)
	#if(im.imageWidth<=doc.width and im.imageHeight<=doc.height):
	if(im.imageWidth<=doc._normalFrame._aW and im.imageHeight<=doc._normalFrame._aH):
		elements.append(im)
	else:
		# reload it if we need to scale it
		if(scaleImagesTo == "Page"):
			pagesize = doc.pagesize
			if(im.imageWidth>im.imageHeight):
				pagesize = doc.pagesize[1],doc.pagesize[0]

			im = Image(localFileName.encode("utf8"), pagesize[0], pagesize[1], kind='proportional', lazy=1)

		frameI = Frame(0, 0, im.drawWidth, im.drawHeight, 0, 0, 0, 0, id="Image " + imageName)
		imageName = imageName.encode("utf-8") # page template things need to be 'string' not 'unicode' for some reason
		pageTemplateName = "Page " + imageName
		doc.addPageTemplates(PageTemplate(id=pageTemplateName, frames=frameI, pagesize=(im.drawWidth,im.drawHeight)))
		elements.append(NextPageTemplate(pageTemplateName))
		if unmodifiedFirstPage == True:
			doc.firstPageWasImage = pageTemplateName
		# add a new page if necessary, this is only necessary for landscape images resized to Page size,
		# with pages with only a small amount of text on them; reportlab is broken in this instance I think
		if modifiedCurrentPage:
			modifiedCurrentPage = newPage(modifiedCurrentPage)
		elements.append(im)
		elements.append(NextPageTemplate("Normal"))

	return False, False

def urlString(text, target):
	return "<a href='%s'><font color='%s'>%s</font></a>" % (target, info.highlightColor, text)

def emitReferences(referenceList, modifiedCurrentPage, isRootPage):
	modifiedCurrentPage = newPage(modifiedCurrentPage)

	if isRootPage:
		addParagraph("References", Heading2Style)
	else:
		addParagraph("References", ReferencesHeading2Style)

	listElements = []
	for i in range(len(referenceList)):
		hrefOrigin = referenceList[i][0]
		hrefReference = referenceList[i][1]
		text = referenceList[i][2] if len(referenceList[i]) > 2 else ""
		listElements.append(Paragraph("<a name='"+hrefReference+"'></a>"+urlString("&nbsp;&nbsp;&uarr;&nbsp;&nbsp;", hrefOrigin)+" "+text, ListPStyle))

	elements.append(ListFlowable(listElements, style=LStyle))

	return True

alterParaPre = []
alterParaPost = []
alterParaPop = 0

reCSSFontSize = compile("font-size:\s*([^\s;]+)\s*[;]*\s*", IGNORECASE | UNICODE)
def repCSSFontSize(match):
	global alterParaPre
	global alterParaPost

	fsp = match.group(1)
	fontsize = 0
	leading = 0

	if fsp.endswith('%'):
		fontsize = float(fsp[:-1])/100*PStyle.fontSize # FIXME: hack! implement a real system of altering this here!
		leading = float(fsp[:-1])/100*PStyle.leading # FIXME: hack! implement a real system of altering this here!

	alterParaPre[-1].append("<para fontSize='%s' leading='%s'>" % (fontsize, leading))
	alterParaPost[-1].insert(0, "</para>")

reCSSBorder = compile("border:\s*[;]*\s*", IGNORECASE)

reCSSFontFamily = compile("font-family:\s*([^\s;]+)\s*[;]*\s*", IGNORECASE)
def repCSSFontFamily(match):
	global alterParaPre
	global alterParaPost

	if match.group(1).lower() == "impact":
		alterParaPre[-1].append("<b>")
		alterParaPost[-1].insert(0, "</b>")
	elif match.group(1).lower() == "calibri":
		pass
	# this is just the 'default' font for email in outlook and such, ignore as arial is fine
	# we could swap with courier if we were feeling 'classic'.
	else:
		print "Unhandled font-family: %r" % match.group(1).lower()

reCSSFontWeight = compile("font-weight:\s*([^\s;]+)\s*[;]*\s*", IGNORECASE)
def repCSSFontWeight(match):
	global alterParaPre
	global alterParaPost

	if match.group(1).lower() == "bold":
		alterParaPre[-1].append("<b>")
		alterParaPost[-1].insert(0, "</b>")
	else:
		print "Unhandled font-weight: %r" % match.group(1).lower()

reCSSColor = compile("color:\s*([^\s;]+)\s*[;]*\s*", IGNORECASE)
def repCSSColor(match):
	global alterParaPre
	global alterParaPost

	alterParaPre[-1].append("font color='%s" % match.group(1))
	alterParaPost[-1].insert(0, "</font>")

class CurrText:
	currentParagraphStyle = [PStyle]

	text = ""
	textStyle = PStyle #currentParagraphStyle[-1]
	textStoredWhileRefIsBeingParsed = []
	justEmittedHeaderOrListFlowable = False # OrCenter ... as well

	def __init__(self):
		pass

	def emit(self):
		#if self.text != "":
		if self.justEmittedHeaderOrListFlowable == True and self.text == "":
			pass
		else:
			#print "%d>>%r" % (len(self.currentParagraphStyle), self.text)
			addParagraph(self.text, self.textStyle)
			self.textStyle = self.currentParagraphStyle[-1]
			self.text = ""
			self.justEmittedHeaderOrListFlowable = False
	def append(self, text):
		self.text += text
	def clear(self):
		self.text = ""
	def appendFixUnicode(self, text):
		self.text += fixUnicode(text)
	def isEmpty(self):
		return self.text == ""

	def addH2(self, modifiedCurrentPage):
		addParagraph(self.text, Heading2Style)
		addHR()
		self.justEmittedHeaderOrListFlowable = True
		return True

	def addH3(self, modifiedCurrentPage):
		addParagraph(self.text, Heading3Style)
		self.justEmittedHeaderOrListFlowable = True
		return True

	def addH4(self, modifiedCurrentPage):
		addParagraph(self.text, Heading4Style)
		self.justEmittedHeaderOrListFlowable = True
		return True


def createPDF(templateName, scaleImagesTo):
	global elements
	global alterParaPre
	global alterParaPost
	global alterParaPop

	# reset everything to null
	del elements[:]
	del alterParaPre[:]
	del alterParaPost[:]
	alterParaPop = 0
	# /reset

	updateHighlightColor()

	if info.filename:
		outputFileName = sanitiseFileName(basePDFOutputDir + info.filename + ".pdf")
	else:
		outputFileName = sanitiseFileName(basePDFOutputDir + info.title + ".pdf")

	doc = LNDocTemplate(outputFileName,pagesize=pagesizes.A4,
						leftMargin=1.2*cm,rightMargin=1.2*cm,
						topMargin=1*cm,bottomMargin=1*cm,
						#pageCompression=1,
						#verbosity=1
						)
	doc.title = info.title
	doc.author = info.story

	modifiedCurrentPage = False
	unmodifiedFirstPage = True

	currText = CurrText()

	referenceList = []
	anonRefUID = 0 # TODO: we need to increment this everytime we parse a new document

	isChapterStart = True

	h1IsNotARealHeaderException = False

	for p in particleList:
		#print "%r %r" % (p.type, p.text if p.text != None else "")

		if(p.type=="text"):
			currText.appendFixUnicode(p.text)
		elif(p.type=="new-paragraph"):
			#if modifiedCurrentPage == True and currText != "":
			if (modifiedCurrentPage == False and currText.isEmpty() == False) or modifiedCurrentPage == True:
				currText.emit()
				unmodifiedFirstPage = False
				modifiedCurrentPage = True
		elif(p.type=="image"):
			if modifiedCurrentPage == True and currText != "":
				currText.emit()
			modifiedCurrentPage, unmodifiedFirstPage = insertImage(
				baseCacheDir + templateName, p.imageName, scaleImagesTo, doc, modifiedCurrentPage, unmodifiedFirstPage)
		elif(p.type=="h1"):
			currText.emit()
		elif(p.type=="/h1"):
			# annoying exception needed for ZnT v1
			if info.headingStyle == "magazine":
				h1IsNotARealHeaderException = True
			if modifiedCurrentPage and not unmodifiedFirstPage:
				modifiedCurrentPage = newPage(modifiedCurrentPage)

			modifiedCurrentPage = currText.addH2(modifiedCurrentPage)
			currText.clear()
		elif(p.type=="h2"):
			currText.emit()
		elif(p.type=="/h2"):
#			print "H2> %r %r" % (h1IsNotARealHeaderException, currText.text)
			if isChapterStart and not h1IsNotARealHeaderException:
				if modifiedCurrentPage and not unmodifiedFirstPage:
					modifiedCurrentPage = newPage(modifiedCurrentPage)
				isChapterStart = False
			h1IsNotARealHeaderException = False

			modifiedCurrentPage = currText.addH2(modifiedCurrentPage)
			currText.clear()
		elif(p.type=="h3"):
			currText.emit()
		elif(p.type=="/h3"):
			if (isChapterStart or info.headingStyle == "magazine") and not h1IsNotARealHeaderException:
#				print "H3> %r %r" % (h1IsNotARealHeaderException, currText.text)
				if modifiedCurrentPage and not unmodifiedFirstPage:
					modifiedCurrentPage = newPage(modifiedCurrentPage)
				isChapterStart = False
			h1IsNotARealHeaderException = False

			if info.headingStyle == "magazine":
				modifiedCurrentPage = currText.addH2(modifiedCurrentPage)
			else:
				modifiedCurrentPage = currText.addH3(modifiedCurrentPage)
			currText.clear()
		elif(p.type=="h4"):
			currText.emit()
		elif(p.type=="/h4"):
			if info.headingStyle == "magazine":
				modifiedCurrentPage = currText.addH3(modifiedCurrentPage)
			else:
				modifiedCurrentPage = currText.addH4(modifiedCurrentPage)
			currText.clear()
		elif(p.type=="dt"):
			currText.emit()
		elif(p.type=="/dt"):
			elements.append(ListFlowable([Paragraph(buildParagraph(currText.text), PStyle)], style=DefinitionListStyle))
			currText.justEmittedHeaderOrListFlowable = True
			currText.clear()
		elif(p.type in ("b", "/b", "i", "/i", "sup", "/sup", "u", "/u", "sub", "/sub", "strike", "/strike")):
			currText.append("<%s>" % p.type)
		elif(p.type in ("strong", "/strong")):
			currText.append("</b>") if (p.type[0] == '/') else currText.append("<b>")
		#elif(p.type=="lt"):
		#	currText.append("&lt;")
		#elif(p.type=="gt"):
		#	currText += "&gt;"
		elif(p.type in ("del", "s")):
			currText.append("<strike>")
		elif(p.type in ("/del", "/s")):
			currText.append("</strike>")
		elif(p.type=="big"):
			currText.append("<font size='+1'>")
		elif(p.type=="/big"):
			currText.append("</font>")
		elif(p.type=="small"):
			currText.append("<font size='-1'>")
		elif(p.type=="/small"):
			currText.append("</font>")
		elif(p.type=="center"):
			if currText.text != "":
				print "Error: <center> found while text-to-be-printed is not empty: %r" % currText.text
				currText.emit()
			currText.currentParagraphStyle.append(CenterPStyle)
			currText.textStyle = currText.currentParagraphStyle[-1]
		elif(p.type=="/center"):
			# TODO: an assert that there's a CenterPStyle on the stack would be good
			currText.currentParagraphStyle.pop()

			currText.emit()
			currText.justEmittedHeaderOrListFlowable = True

		elif(p.type=="ref" or p.type.startswith("ref ")):
			refId = str(len(referenceList)+1)
			hrefNameBase = str(anonRefUID)+"_"+refId
			hrefNameOrigin = hrefNameBase+"_origin"
			hrefNameReference = hrefNameBase+"_reference"
			referenceList.append((hrefNameOrigin, hrefNameReference))

			currText.append("<a name='"+hrefNameOrigin+"'></a><sup>"+urlString("["+refId+"]", hrefNameReference)+"</sup>")
			currText.textStoredWhileRefIsBeingParsed.append(currText.text)
			currText.clear()

		elif(p.type=="/ref"):
			if len(referenceList[-1]) != 2:
				print "Error in reference handling code: %r %r %r" % (referenceList[-1])
			hrefNameOrigin = referenceList[-1][0]
			hrefNameReference = referenceList[-1][1]
			refData = currText.text
			referenceList[-1] = (hrefNameOrigin, hrefNameReference, refData)
			currText.text = currText.textStoredWhileRefIsBeingParsed[-1]
			currText.textStoredWhileRefIsBeingParsed.pop()

		elif(p.type=="reference-location"):
			if currText.text != "":
				print "Error: <reference-location> found while text-to-be-printed is not empty: %r" % currText.text
			# I assume if we've got this far that we actually are on the root page... even if there's no
			# information stored related to this...
			if info.referencePlacement == "root" and len(referenceList) > 0:
				modifiedCurrentPage = emitReferences(referenceList, modifiedCurrentPage, True)
				referenceList = []
				anonRefUID += 1

		elif(p.type=="new-chapter"):
			isChapterStart = True

		elif(p.type=="end-chapter"):
			currText.emit()

			if info.referencePlacement == "chapter" and len(referenceList) > 0:
				modifiedCurrentPage = emitReferences(referenceList, modifiedCurrentPage, False)
				referenceList = []
				anonRefUID += 1

		elif(p.type.startswith("a ")):
			currText.append("<%s><font color='%s'>" % (p.type, info.highlightColor))
		elif(p.type=="/a"):
			currText.append("</font></a>")

		elif(p.type=="br" or p.type=="br/" or p.type=="br /"):
			currText.append("<br/>")

		elif(p.type=="hr"):
			currText.emit()
			if currText.text != "":
				print "Error: <hr> found while text-to-be-printed is not empty: %r" % currText.text
			addParagraph("", PStyle)
			addHR()

		elif(p.type=="new-chapter"):
			pass
		elif(p.type=="ruby" or p.type=="/ruby"):
			# we shouldn't need to emit anything for these since we're not handling ruby characters properly
			pass
		elif(p.type=="rt"):
			currText.append("(")
		elif(p.type=="/rt"):
			currText.append(")")
		elif(p.type in ("span", "div", "p")):
			style = p.style
			alterParaPre.append([])
			alterParaPost.append([])

			style = sub(reCSSFontSize, repCSSFontSize, style)
			style = sub(reCSSBorder, "", style) # I'm not currently handling this properly since they're not using it properly
			style = sub(reCSSFontFamily, repCSSFontFamily, style)
			style = sub(reCSSFontWeight, repCSSFontWeight, style)
			style = sub(reCSSColor, repCSSColor, style)

			if style != "":
				print "Missing css-style options: %r" % style
		elif(p.type in ("/span", "/div", "/p")):
			# if the text is empty, we'll just strip off the spans as is, otherwise we need to wait until
			# we print them
			if currText.isEmpty():
				alterParaPre.pop();
				alterParaPost.pop();
			else:
				alterParaPop += 1
		else:
			#else:
			print "%r" % p.type

		# little bit of a hack to get a good front cover
		if modifiedCurrentPage == True:
			unmodifiedFirstPage = False

	# last bit of cleanup
	currText.emit()
	#if currText != "":
		#addParagraph(currText, PStyle)
	modifiedCurrentPage = appendDisclaimer(doc, modifiedCurrentPage)

	doc.build(elements)
