#!/usr/bin/env python

import os.path
import zipfile
import codecs
import re
import datetime

from particles import Particle, particleList
from config import info, baseEPUBOutputDir, baseCacheDir
from filehandling import sanitiseFileName

sectionTemplate = \
'''<?xml version="1.0" encoding="UTF-8"?><html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
<head>
	<title>%s</title>
	<style type="text/css">
		h1, h2, h3, h4, h5, h6 { color: %s; }
		h1, h2 { border-bottom: 1px solid #AAAAAA; }
		h3 { text-align: center; }
		img { vertical-align: middle; max-width: 100%%; max-height: 100%%; }
		a:link, a:visited, a:hover, a:active { color: %s; }
	</style>
</head>
<body>
	<section class="body-rw Chapter-rw" epub:type="bodymatter chapter">
	%s
	</section>
</body>
</html>
'''

htmlBody = ""

def addParagraph(txt, outputAside):#, style):
	global htmlBody
	#txt = fixUnicode(txt)
	# ignore style for now...
	htmlBody += "<p>%s</p>%s\n" % (txt, outputAside)

def urlString(text, target):
	return "<a href='%s'>%s</a>" % (target, text)


def emitReferences(referenceList, modifiedCurrentPage, isRootPage):
	global htmlBody
	#modifiedCurrentPage = newPage(modifiedCurrentPage)

	#if isRootPage:
	#addParagraph("References", Heading2Style)
	htmlBody += "<h2>References</h2>"
	#else:
	#	addParagraph("References", ReferencesHeading2Style)

	htmlBody += "<ol>"
	listElements = []
	for i in range(len(referenceList)):
		hrefOrigin = referenceList[i][0]
		hrefReference = referenceList[i][1]
		text = referenceList[i][2] if len(referenceList[i]) > 2 else ""
		#text = fixUnicode(referenceList[i][2]) if len(referenceList[i]) > 2 else ""
		htmlBody += "<li>"+"<a id='"+hrefReference+"'></a>"+urlString("&nbsp;&nbsp;&uarr;&nbsp;&nbsp;", "#"+hrefOrigin)+" "+text+"</li>"
	htmlBody += "</ol>"

	return True

def createEPUB(templateName, iOS):
	global htmlBody

	outputFileName = sanitiseFileName(baseEPUBOutputDir + info.title + ".epub")
	epub = zipfile.ZipFile(outputFileName, 'w')
	
	# The first file must be named "mimetype"
	epub.writestr("mimetype", "application/epub+zip")
	
	htmlFiles = []
	imgFiles = []
	
	sectionNumber = 0
	sectionName = ""
	sectionTitle = ""
	
	modifiedCurrentPage = False
	unmodifiedFirstPage = True

	#currentParagraphStyle = [PStyle]

	currText = ""
	outputAside = ""
	#currTextStyle = currentParagraphStyle[-1]
	currTextStoredWhileRefIsBeingParsed = []

	noWiki = False

	referenceList = []
	anonRefUID = 0 # TODO: we need to increment this everytime we parse a new document
	
	isChapterStart = True

	for p in particleList:
		if(p.type=="text"):
			currText += p.text
			modifiedCurrentPage = True
		elif(p.type=="new-paragraph"):
			if modifiedCurrentPage == True and currText != "":
				addParagraph(currText, outputAside)#, currTextStyle)
				#currTextStyle = currentParagraphStyle[-1]
				currText = ""
				outputAside = ""
		#elif(p.type=="image"):
		#	modifiedCurrentPage, unmodifiedFirstPage = insertImage(
		#		baseCacheDir + templateName, p.imageName, scaleImagesTo, doc, modifiedCurrentPage, unmodifiedFirstPage)
		elif(p.type=="h2"):
			#if info.headingStyle == "Magazine":
			#	modifiedCurrentPage = newPage(modifiedCurrentPage)
			currTextStoredWhileRefIsBeingParsed.append(currText)
			currText = ""
		elif(p.type=="/h2"):
			if isChapterStart and htmlBody != "":
				sectionName = "section_%d.html" % sectionNumber
				htmlFiles.append((str(sectionNumber), sectionTitle, sectionName))
				epub.writestr('OEBPS/'+sectionName, (sectionTemplate % (sectionTitle, info.highlightColor, info.highlightColor, htmlBody)).encode('utf-8'))
				htmlBody = ""
				sectionNumber += 1
				isChapterStart = False
				
			sectionTitle = currText
			htmlBody += "<h2>%s</h2>" % currText
			modifiedCurrentPage = True
			currText = currTextStoredWhileRefIsBeingParsed[-1]
			currTextStoredWhileRefIsBeingParsed.pop()
		elif(p.type=="h3"):
			currTextStoredWhileRefIsBeingParsed.append(currText)
			currText = ""
		elif(p.type=="/h3"):
			if isChapterStart and htmlBody != "" and info.headingStyle == "magazine":
				sectionName = "section_%d.html" % sectionNumber
				htmlFiles.append((str(sectionNumber), sectionTitle, sectionName))
				epub.writestr('OEBPS/'+sectionName, (sectionTemplate % (sectionTitle, info.highlightColor, info.highlightColor, htmlBody)).encode('utf-8'))
				htmlBody = ""
				sectionNumber += 1
				isChapterStart = False
				
			sectionTitle = currText
			if info.headingStyle == "magazine":
				htmlBody += "<h2>%s</h2>" % currText
				modifiedCurrentPage = True
			else:
				htmlBody += "<h3>%s</h3>" % currText
				modifiedCurrentPage = True
				
			currText = currTextStoredWhileRefIsBeingParsed[-1]
			currTextStoredWhileRefIsBeingParsed.pop()
		elif(p.type=="h4"):
			currTextStoredWhileRefIsBeingParsed.append(currText)
			currText = ""
		elif(p.type=="/h4"):
			if info.headingStyle == "magazine":
				htmlBody += "<h3>%s</h3>" % currText
				modifiedCurrentPage = True
			else:
				htmlBody += "<h4>%s</h4>" % currText
				modifiedCurrentPage = True
				
			currText = currTextStoredWhileRefIsBeingParsed[-1]
			currTextStoredWhileRefIsBeingParsed.pop()
		elif(p.type=="dt"):
			currTextStoredWhileRefIsBeingParsed.append(currText)
			currText = ""
		elif(p.type=="/dt"):
			#currText = fixUnicode(currText)
			# bit of a hack, we should be working out where the start/end of the dl is.
			htmlBody += "<dl><dt>%s</dt></dl>" % currText
			
			currText = currTextStoredWhileRefIsBeingParsed[-1]
			currTextStoredWhileRefIsBeingParsed.pop()
		elif(p.type=="b"):
			currText += "<b>"
		elif(p.type=="/b"):
			currText += "</b>"
		elif(p.type=="i"):
			currText += "<i>"
		elif(p.type=="/i"):
			currText += "</i>"
		elif(p.type=="sup"):
			currText += "<sup>"
		elif(p.type=="/sup"):
			currText += "</sup>"
		elif(p.type=="lt"):
			currText += "&lt;"
		elif(p.type=="gt"):
			currText += "&gt;"
		elif(p.type=="big"):
			currText += "<font size='+1'>"
		elif(p.type=="/big"):
			currText += "</font>"
		elif(p.type=="center"):
			currText += "<span style='text-align: center;'>"
		elif(p.type=="/center"):
			currText += "</span>"
		elif(p.type=="span"):
			currText += "<span style='%s;'>" % p.style
		elif(p.type=="/span"):
			currText += "</span>"

		elif(p.type=="ref" or p.type.startswith("ref ")):
			refId = str(len(referenceList))
			hrefNameBase = str(anonRefUID)+"_"+refId
			hrefNameOrigin = hrefNameBase+"_origin"
			hrefNameReference = hrefNameBase+"_reference"
			referenceList.append((hrefNameOrigin, hrefNameReference))

			currText += "<sup><a epub:type='noteref' href='#n%s'>%s</a></sup>" % (hrefNameReference, refId)# "<a id='"+hrefNameOrigin+"'></a><sup>"+urlString("["+refId+"]", "#"+hrefNameReference)+"</sup>"
			#currText += "<a id='"+hrefNameOrigin+"'></a><sup>"+urlString("["+refId+"]", "#"+hrefNameReference)+"</sup>"
			currTextStoredWhileRefIsBeingParsed.append(currText)
			currText = ""

		elif(p.type=="/ref"):
			if len(referenceList[-1]) != 2:
				print "Error in reference handling code."
			hrefNameOrigin = referenceList[-1][0]
			hrefNameReference = referenceList[-1][1]
			refData = currText
			referenceList[-1] = (hrefNameOrigin, hrefNameReference, refData)
			currText = currTextStoredWhileRefIsBeingParsed[-1]
			currTextStoredWhileRefIsBeingParsed.pop()
			outputAside += "<aside epub:type='footnote' id='n%s' style='display: none;'>%s</aside>" % (hrefNameReference, refData)

		elif(p.type=="reference-location"):
			if currText != "":
				print "Error: <reference-location> found while text-to-be-printed is not empty: %r" % currText
			# I assume if we've got this far that we actually are on the root page... even if there's no
			# information stored related to this...
			#if info.referencePlacement == "root" and len(referenceList) > 0:
			#	modifiedCurrentPage = emitReferences(referenceList, modifiedCurrentPage, True)
			#	referenceList = []
			#	anonRefUID += 1

		elif(p.type=="new-chapter"):
			isChapterStart = True
			
		elif(p.type=="end-chapter"):
			# same three lines as for new-paragraph
			if currText != "":
				addParagraph(currText, outputAside)
				#currTextStyle = currentParagraphStyle[-1]
				currText = ""
				outputAside = ""

			#if info.referencePlacement == "chapter" and len(referenceList) > 0:
			#	modifiedCurrentPage = emitReferences(referenceList, modifiedCurrentPage, False)
			#	referenceList = []
			#	anonRefUID += 1

		elif(p.type.startswith("a ")):
			currText += "<%s>" % (p.type)
		elif(p.type=="/a"):
			currText += "</a>"

		elif(p.type=="br" or p.type=="br/" or p.type=="br /"):
			currText += "<br/>"

		elif(p.type=="hr"):
			if currText != "":
				print "Error: <hr> found while text-to-be-printed is not empty: %r" % currText
			currText += "<p></p><hr/>"
			#addParagraph("", currTextStyle)
			#addHR()

		elif(p.type=="nowiki"):
			if noWiki == True:
				print "Error: <nowiki> found while <nowiki> was already enabled"
			noWiki = True
		elif(p.type=="/nowiki"):
			if noWiki == False:
				print "Error: </nowiki> found while <nowiki> was already disabled"
			noWiki = False
		else:
			print "%r" % p.type
				
	if htmlBody != "":
		sectionName = "section_%d.html" % sectionNumber
		htmlFiles.append((str(sectionNumber), sectionTitle, sectionName))
		epub.writestr('OEBPS/'+sectionName, (sectionTemplate % (sectionTitle, info.highlightColor, info.highlightColor, htmlBody)).encode('utf-8'))
		htmlBody = ""
		sectionNumber += 1
		isChapterStart = False
		
	##print "%r" % htmlBody
	#f = codecs.open('temp.html', 'wb', 'utf-8')
	#f.write(sectionTemplate % (info.title, info.highlightColor, info.highlightColor, htmlBody))
	#
	#f.close()

	# We need an index file, that lists all other HTML files
	# This index file itself is referenced in the META_INF/container.xml
	# file
	epub.writestr("META-INF/container.xml", \
'''<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
	<rootfiles>
		<rootfile full-path="OEBPS/Content.opf" media-type="application/oebps-package+xml"/>
	</rootfiles>
</container>''');
	
	# The index file is another XML file, living per convention
	# in OEBPS/Content.xml
	index_tpl = \
'''<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<package version="3.0" xmlns="http://www.idpf.org/2007/opf" unique-identifier="%(uid)s">
	<metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
		<dc:identifier id="%(uid)s">%(uid)s</dc:identifier>
		<dc:title>%(title)s</dc:title>
		<dc:creator>%(story)s</dc:creator>
		<dc:language>en</dc:language>
		<meta name="cover" content="%(cover)s" />
		<meta property="dcterms:modified">%(date)s</meta>
	</metadata>
	<manifest>
		<item id="file_-1" properties="nav" href="toc.html" media-type="application/xhtml+xml"/>
		%(manifest)s
	</manifest>
	<spine toc="ncx">
		%(spine)s
		</spine>
</package>'''
	
	toc_tpl = \
'''<?xml version="1.0" encoding="UTF-8"?><html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
<head>
	<title>%(title)s</title>
	<style type="text/css">
		.tocEntry { }
	</style>
</head>
<body>
	%(toc)s
</body>
</html>'''

	manifest = ""
	spine = ""
	toc = ""
	# Write each HTML file to the ebook, collect information for the index
	for item in htmlFiles:
		manifest += '<item id="file_%s" href="%s" media-type="application/xhtml+xml"/>' % (
					  item[0], item[2])
		spine += '<itemref idref="file_%s" />' % (item[0])
		toc += "<div class='tocEntry'><a href='%s'>%s</a></div>" % (item[2], item[1])
	
	# Finally, write the index
	epub.writestr('OEBPS/Content.opf', index_tpl % {
		'cover': "", #FIXME: support specifying cover!
		'story': info.story,
		'title': info.title,
		'uid': re.sub("[^A-Za-z0-9]", ".", templateName),
		'manifest': manifest,
		'spine': spine,
		'date': datetime.datetime.now().ctime()
	})
	epub.writestr('OEBPS/toc.html', toc_tpl % {
		'title': info.title,
		'toc': toc,
	})
	return

