#!/usr/bin/env python

import os
import sys
from re import compile, sub, IGNORECASE, DOTALL, MULTILINE, match, UNICODE, search
from config import baseCacheDir, baseTemplateDir, info
from particles import Particle, particleList
from filehandling import sanitiseFileName, grabPage, sanitiseFileNameCompletely

import html5lib
from html5lib import treebuilders, treewalkers, serializer
from html5lib.filters import sanitizer

reNoInclude = compile("(<noinclude>.*?</noinclude>[\n]*)", IGNORECASE | DOTALL)
reIncludeOnly = compile("<includeonly>(.*?)</includeonly>", IGNORECASE | DOTALL)
reComment   = compile("(<!--.*?-->)", IGNORECASE | DOTALL)

reReferences = compile("<references[\s]*(group=[^/]*)*/>[\n]*", IGNORECASE)
reRefHeader1 = compile("==Translator's Notes (and|&) References==[\n]*", IGNORECASE)
reRefHeader2 = compile("[=]*==Translation Notes( (and|&) References)*==[=]*[\n]*", IGNORECASE)
reRefHeader3 = compile("===[=]*[\s]*Notes[\s]*[=]*===[\n]*", IGNORECASE)
reRefHeader4 = compile("==References( and Translation Notes)*==[\n]*", IGNORECASE)

reHTML       = compile("<([^<]*?)>", IGNORECASE | DOTALL)

reWikiBoldItalic = compile("'''''(.+?)'''''", IGNORECASE | DOTALL)
reWikiBold       = compile("'''(.+?)'''", IGNORECASE | DOTALL)
reWikiItalic     = compile("''(.+?)''(?!')", IGNORECASE | DOTALL)

reAndSemi        = compile("&([a-zA-Z]+);", IGNORECASE | DOTALL)

reLinkExternal   = compile("\[(http[s]*[^ \]]+)[ ]*([^\]]*)\]", IGNORECASE | DOTALL)
reLinkUser       = compile("\[\[User:[\s]*([^|\]]+)[|]*([^\]]*)\]\]", IGNORECASE | DOTALL) #ugg! This is ugly!
reLinkWikia      = compile("\[\[wikia:c:([^:]*?):[\s]*([^|\]]+)[|]*([^\]]*)\]\]", IGNORECASE | DOTALL) #ugg! This is ugly!

# ignore the ASCII-ish characters, and also the en/em-dashes and horizontal elipsis
reNonEnglishChars = compile(u"([^\u0000-\u00FF\u2013\u2014\u2026]+)")

def urlString(text, target):
	if( (text is None) or (text == '') ):
		text = target

	print "%r     %r" % (text, target)
	return "<a href='%s'>%s</a>" % (target.replace("'", "\\'"), text.replace("'", "\\'"))

def sanitize(text):
	# strip the <noinclude> ... </noinclude> text
	text = sub(reNoInclude, "", text)

	# strip comments
	text = sub(reComment, "", text)

	# strip any excess spaces at the beginning/end of page
	text = text.strip()

	# get rid of __TOC__ for the moment too...
	text = text.replace("__TOC__\n", "")

	# we strip out some crappy formatting, fancy unicode quotes and the like
	# they're not used consistantly so they're better off not in there so it doesn't look odd
	# TODO: later on properly replace them with the appropriate ones in an intelligent fashion
	text = text.replace(u"\u2018", u"'") # LEFT SINGLE QUOTATION MARK
	text = text.replace(u"\u2019", u"'") # RIGHT SINGLE QUOTATION MARK
	text = text.replace(u"\u201C", u'"') # LEFT DOUBLE QUOTATION MARK
	text = text.replace(u"\u201D", u'"') # RIGHT DOUBLE QUOTATION MARK

	# remove all the default included 'reference headers' and html tag locations to output the contents
	text = sub(reReferences, "", text)
	text = sub(reRefHeader1, "", text)
	text = sub(reRefHeader2, "", text)
	text = sub(reRefHeader3, "", text)
	text = sub(reRefHeader4, "", text)

	# replace HTML tags with faux {{html}} tags, and fixup the stand-alone &/</> symbols
	text = text.replace("<<", "&lt;&lt;")
	text = text.replace(">>", "&gt;&gt;")
	text = sub(reHTML, lambda match: "{{%s}}" % match.group(1).lower(), text)
	text = sub(reAndSemi, lambda match: "{{%s}}" % match.group(1).lower(), text)
	text = text.replace("&", "&amp;")
	text = text.replace("<", "&lt;")
	text = text.replace(">", "&gt;")

	# \u3000 spaces should be double wide &nbsp; spaces, but for some reason our pdf renderer isn't doing it properly
	# FIXME: probably should handle this in the createpdf.py file rather then in the loader.
	#text = text.replace(u"\u3000", u"&nbsp;&nbsp;")

	# strip some of the wiki link crud and replace it with more sane stuff
	text = sub(reWikiBoldItalic, lambda match: "{{b}}{{i}}%s{{/i}}{{/b}}" % match.group(1), text)
	text = sub(reWikiBold,	   lambda match: "{{b}}%s{{/b}}" % match.group(1), text)
	text = sub(reWikiItalic,	 lambda match: "{{i}}%s{{/i}}" % match.group(1), text)

	text = sub(reLinkExternal, lambda match: urlString(match.group(2), match.group(1)), text)
	text = sub(reLinkUser,
				  lambda match: urlString(match.group(2), "http://www.baka-tsuki.org/project/index.php?title=User:"+match.group(1)),
				  text)
	text = sub(reLinkWikia,
				  lambda match: urlString(match.group(3), "http://%s.wikia.com/wiki/%s" % (match.group(1), match.group(2))),
				  text)

	#text = sub(reNonEnglishChars, lambda match: "{{i18n}}%s{{/i18n}}" % match.group(1), text)

	return text

def loadAndPreparePage(pagePath):
	text = open(sanitiseFileName(pagePath),'r').read()

	# interpret as utf8
	text = text.decode('utf-8')

	text = sanitize(text)

	#text = cleanupFormatting(text)

	return text

def parseImage(text):
	global particleList

	colon = text.find(':')
	pipe = text.find('|', colon)
	bracket = text.find(']', colon)

	img = text[(colon+1) : (pipe if (pipe != -1 and pipe<bracket) else bracket)]
	if(not (img.endswith("jpg") or img.endswith("jpeg") or img.endswith("png") or img.endswith("gif"))):
		sys.exit("Invalid image name: " + img)

	p = Particle("image")
	p.imageName = img.strip()

	particleList.append(p)

	bracket = text.find(']]')

	return text[(bracket+2) : ]

def includePageTemplate(text, cacheDir):
	colon = text.find(':')
	pipe = text.find('|', colon)
	brace = text.find('}', colon)

	pageTemplate = text[(colon+1) : (pipe if (pipe != -1 and pipe<brace) else brace)]

	particleList.append(Particle("new-chapter"))

	parsePage(pageTemplate, cacheDir, False)

	particleList.append(Particle("end-chapter"))

	brace = text.find('}}')

	return text[(brace+2) : ]

def appendText(text, off):
	global particleList

	if(off==0):
		return text

	p = Particle("text")
	p.text = text[0 : off]
	particleList.append(p)

	return text[(off) : ]

def parseH2(text, isChapterStart):
	endEq = text.find("==", 2)

	p = None
	if isChapterStart:
		p = Particle("chapter-start-header")
	else:
		p = Particle("h2")

	p.text = text[2 : endEq]
	particleList.append(p)

	return text[(endEq+3) : ]

def parseH3(text, isChapterStart):
	endEq = text.find("===", 3)

	p = None
	if info.headingStyle == "Magazine":
		if isChapterStart:
			p = Particle("chapter-start-header")
		else:
			p = Particle("h2")
	else:
		p = Particle("h3")
	p.text = text[3 : endEq]
	particleList.append(p)

	return text[(endEq+4) : ]

def parseH4(text):
	endEq = text.find("====", 4)

	p = Particle("h4") if info.headingStyle != "Magazine" else Particle("h3")
	p.text = text[4 : endEq]
	particleList.append(p)

	return text[(endEq+5) : ]

def handleTag(text):
	endBrace = text.find("}}", 2)

	p = Particle(text[2 : endBrace])
	particleList.append(p)

	return text[(endBrace+2) : ]

reReferencePlacement   = compile("Reference Placement: (Root|Chapter)", IGNORECASE)
reHighlightColor       = compile("^Highlight Color: (.*)$", IGNORECASE | MULTILINE)
reHeadingStyle         = compile("Heading Style: (Magazine|Novel)", IGNORECASE)
reIncludePageTemplate  = compile("{{:([^|}]+)[|]*[^}]*}}")
reReferencesLocation   = compile("{{==ReferencesLocation}}", IGNORECASE)
reInfo                 = compile("^(Title|Story|Illustrator|Translator|Editor|PDF Maker|Filename):\s+(.*?)$", IGNORECASE | MULTILINE)
reH4                   = compile("^====(.+)====[ \t]*$", IGNORECASE | MULTILINE)
reH3                   = compile("^===(.+)===[ \t]*$", IGNORECASE | MULTILINE)
reH2                   = compile("^==(.+)==[ \t]*$", IGNORECASE | MULTILINE)
reH1                   = compile("^=(.+)=[ \t]*$", IGNORECASE | MULTILINE)
reNewParagraph         = compile("\n[\n]+", IGNORECASE | MULTILINE)
reSpaces               = compile("^ *\n? +$")
reHR                   = compile("^-{4,}[ ]*\n", MULTILINE)
reDefinitionList       = compile("^:(.*)$", MULTILINE) # TODO: handle multiple levels of indentation rather then only a single one.
reFurigana             = compile("{{[\s]*Furigana\|([^\|]+?)\|([^\|}]+).*?}}", IGNORECASE)

reImageLinkBegin       = compile("\[\[(?:Image|File):(.*?\]\])", IGNORECASE | MULTILINE)
reImageLinkEnd         = compile("^(.+?)\]\]$")
reImageLinkMiddle      = compile("^[\s]*([^|\]]+\.(?:jpg|jpeg|png|gif)(?=[\s\]\|]))(.*)$", IGNORECASE)

reCleanWikiMarkupInRef     = compile("<ref(.*?)>(.*?)</ref>", IGNORECASE | MULTILINE)
reEscapeWikiMarkupInNoWiki = compile("<nowiki>(.*?)</nowiki>", IGNORECASE | MULTILINE)

reSpaces2             = compile("\n +\n")
reTag                 = compile("<([^<]*?)>", IGNORECASE | MULTILINE)
reValidSimpleTags     = compile("^(h1|h2|h3|h4|h5|h6|ref|center|i|b|strong|dt|sup|nowiki|ruby|rt|big|small|u|sub|del|strike|s|hr|hr/|br|br[ ]*/|html|head|body|p)$", IGNORECASE | MULTILINE)
reValidComplexTags    = compile("^(a\s href|span\s|div\s|ref\s|p\s|font\s|!--|/)", IGNORECASE | MULTILINE | UNICODE)

# matches the gibberish profanities
reGibberish           = compile("[`~!@#$%^&*\(\)\-=_+\\[\\]\\{\\}\\\\|;:,/\\?]{4,}")

reDualPointyBrackets  = compile("<<(.*?)>>", IGNORECASE | MULTILINE)

reGallery             = compile("<gallery>(.*?)</gallery>[ \t\n]*", IGNORECASE | DOTALL | MULTILINE)

def parseImageLink(match):
	returnString = ""

#	if match.group(1) != "":
#		returnString += match.group(1)

	match2 = reImageLinkEnd.match(match.group(1))

	match3 = reImageLinkMiddle.match(match2.group(1))

	if match3 == None:
		returnString += "<local-image name='%s'/>" % match2.group(1)
	else:
		returnString += "<local-image name='%s'/>" % match3.group(1)

#	if match2.group(2) != "":
#		returnString += match2.group(2).strip()

	return returnString.strip()

def cleanWikiMarkupInRef(match):
	text = match.group(2)

	text = sub(reWikiBoldItalic, lambda match: "<b><i>%s</i></b>" % match.group(1), text)
	text = sub(reWikiBold,	     lambda match: "<b>%s</b>" % match.group(1), text)
	text = sub(reWikiItalic,	 lambda match: "<i>%s</i>" % match.group(1), text)

	#print "REF: %r" % text
	return "<ref>%s</ref>" % text

def newParagraph(match):
	returnString = ""

	text = match.group(0)
	textSize = len(text)

	while(textSize>0):
		returnString += "<new-paragraph></new-paragraph>"
		textSize -= 2

	return returnString

def escapeWikiMarkupInNoWiki(match):
	text = match.group(1)

	# this isn't an error. We need to escape it once for our html parser, then once again for the
	# renderer at the other end, be it an epub/html browser, or the pdf generator
	text = text.replace("&", "&amp;amp;")
	text = text.replace("'", "&amp;apos;")
	text = text.replace("<", "&amp;lt;")
	text = text.replace(">", "&amp;gt;")
	text = text.replace("-", "&amp;#x2D;")

	#print "NOWIKI: %r" % text
	# we don't need to re-surround it with <nowiki></nowiki> tags since they've done their job now
	return "%s" % text

def tempIO(match):
	print match.group(1)
	return match.group(1)

def tag(text):
	tagtext = text.group(1)

	if((match(reValidSimpleTags, tagtext) != None) or (match(reValidComplexTags, tagtext) != None)):
		return "<%s>" % tagtext
	else:
		print "%r" % tagtext
		return "&amp;lt;%s&amp;gt;" % tagtext

def gibberish(match):
	text = match.group(0)

	text = text.replace("&", "&amp;amp;")

	return text

#reXAndY = compile("(?:[\W])([A-Z])&([A-Z])(?:[\W])")
reXAndY = compile("([A-Z])&([A-Z])")
reRightArrow = compile("(?:[^-])->")
reLeftArrow = compile(ur"<[-\u2013]") #"<-(?:[^-])"
def bruteForceReplace(text):
	# FIXME: just handling stuff that I can't be bothered fixing in the wiki at the moment
	text = sub(reXAndY, lambda match: "%s&amp;amp;%s" % ( match.group(1), match.group(2) ), text)
	text = text.replace("__NOTOC__\n", "")
	text = text.replace("<_<", "&amp;lt;_&amp;lt;")
	text = text.replace(">_<", "&amp;gt;_&amp;lt;")
#	text = text.replace(">.<", "&amp;gt;.&amp;lt;")
	text = text.replace(" > < ", " &amp;gt; &amp;lt; ")
	text = text.replace(" >< ", " &amp;gt;&amp;lt; ")
	text = sub(reRightArrow, "-&amp;amp;gt;", text)
	text = sub(reLeftArrow, "&amp;amp;lt;-", text)
	text = text.replace("'<'", "'&amp;lt;'")


	return text

reRedirect = compile("#REDIRECT", IGNORECASE | DOTALL)

#reHR                   = compile("^-{4,}[ ]*\n", MULTILINE)
reNotEndComment = compile("(-{3,})>", IGNORECASE | DOTALL)

def fnord(match):
	print "found: %s" % match.group(0)
	return "%s&amp;gt;" % match.group(1)

def parsePage(templateName, cacheDir, reloadPageCache, isRoot):
	global particleList
	global info

	if not isRoot:
		grabPage(cacheDir, templateName, reloadPageCache)
		text = open(cacheDir.decode('utf8') + "/" + sanitiseFileNameCompletely(templateName),'r').read()
	else:
		text = open(sanitiseFileName(templateName),'r').read()
	text = text.decode('utf-8')

	if search(reRedirect, text) != None:
		sys.exit("Redirect Found!: %s" % templateName)

	# filter things that look like end comments
#	text = sub(reNotEndComment, lambda match: "%s&amp;gt;" % match.group(1), text)
	#text = sub(reNotEndComment, fnord, text)

	# strip comments
	text = sub(reComment, "", text)

	# strip gallery
	text = sub(reGallery, "", text)

	# strip the <noinclude> ... </noinclude> text, and include the <includeonly> text
	text = sub(reNoInclude, "", text)
	text = sub(reIncludeOnly, lambda match: match.group(1), text)

	# remove all the default included 'reference headers' and html tag locations to output the contents
	text = sub(reReferences, "", text)
	text = sub(reRefHeader1, "", text)
	text = sub(reRefHeader2, "", text)
	text = sub(reRefHeader3, "", text)
	text = sub(reRefHeader4, "", text)

	# get rid of __TOC__ for the moment too...
	text = text.replace("__TOC__\n", "")

	# there really shouldn't be any need to handle this
	text = text.replace("<br style=\"clear:both\"/>", "")

	text = text.replace("&amp;", "&amp;amp;")
	text = text.replace("&lt;",  "&amp;lt;")

	# this should get rid of any problems with people writing "X & Y" without having to parse a whole
	# chunk of stuff
	text = text.replace(" & ", " &amp;amp; ")
	text = sub(reGibberish, gibberish, text)

	# we strip out some crappy formatting, fancy unicode quotes and the like
	# they're not used consistently so they're better off not in there so it doesn't look odd
	# TODO: later on properly replace them with the appropriate ones in an intelligent fashion
	text = text.replace(u"\u2018", u"'") # LEFT SINGLE QUOTATION MARK
	text = text.replace(u"\u2019", u"'") # RIGHT SINGLE QUOTATION MARK
	text = text.replace(u"\u201C", u'"') # LEFT DOUBLE QUOTATION MARK
	text = text.replace(u"\u201D", u'"') # RIGHT DOUBLE QUOTATION MARK
	text = text.replace(u"\u2010", u'-') # HYPHEN

	text = sub(reEscapeWikiMarkupInNoWiki, escapeWikiMarkupInNoWiki, text)
	text = sub(reCleanWikiMarkupInRef,     cleanWikiMarkupInRef, text)

	text = sub(reDualPointyBrackets, lambda match: "&amp;lt;&amp;lt;%s&amp;gt;&amp;gt;" % match.group(1), text)
	text = text.replace("<<", "&amp;lt;&amp;lt;")
	text = text.replace(">>", "&amp;gt;&amp;gt;")
	text = text.replace("<3", "&hearts;") # ... yeah, pointless hack. <3
	#text = text.replace("<-", "&amp;amp;lt;-")
	text = sub(reLeftArrow, "&amp;amp;lt;-", text)

	# filter out all the things that look like html tags but aren't.
	text = sub(reTag, tag, text)

	text = sub(reWikiBoldItalic, lambda match: "<b><i>%s</i></b>" % match.group(1), text)
	text = sub(reWikiBold,	     lambda match: "<b>%s</b>" % match.group(1), text)
	text = sub(reWikiItalic,	 lambda match: "<i>%s</i>" % match.group(1), text)

	text = sub(reFurigana, lambda match: "<ruby>%s<rt>%s</rt></ruby>" % (match.group(1), match.group(2)), text)

	text = sub(reReferencePlacement,
			   lambda match: "<reference-placement location='%s'/>" % match.group(1).lower(), text)
	text = sub(reHighlightColor,
			   lambda match: "<highlight-color color='%s'/>" % match.group(1).lower(), text)
	text = sub(reHeadingStyle,
			   lambda match: "<heading-style style='%s'/>" % match.group(1).lower(), text)
	text = sub(reIncludePageTemplate,
			   lambda match: "<include-page-template title='%s'/>" % match.group(1).replace("'", "%27"), text) # ' -> %27 is a bit of a hack
	text = sub(reReferencesLocation,
			   lambda match: "<references-summary/>", text)
	text = sub(reInfo,
			   lambda match: "<info type='%s' value='%s'/>" % (match.group(1).lower(), match.group(2).replace("'", "&apos;")), text)

	text = sub(reH4, lambda match: "<h4>%s</h4>" % match.group(1), text)
	text = sub(reH3, lambda match: "<h3>%s</h3>" % match.group(1), text)
	text = sub(reH2, lambda match: "<h2>%s</h2>" % match.group(1), text)
	text = sub(reH1, lambda match: "<h1>%s</h1>" % match.group(1), text)

	text = sub(reDefinitionList, lambda match: "<dt>%s</dt>" % match.group(1), text)
	#text = sub(reFurigana, lambda match: "<ruby>%s<rt>%s</rt></ruby>" % (match.group(1), match.group(2)), text)

	text = sub(reHR, lambda match: "<hr/>", text)

	text = sub(reImageLinkBegin, parseImageLink, text)

	# I can't think of any reason for there to be tabs in the code...
	text = text.replace("\t", "")
	# also strip any odd spaces on a line with nothing else
	text = sub(reSpaces2, "\n\n", text)

	text = sub(reNewParagraph, newParagraph, text)

	text = sub(reLinkExternal, lambda match: urlString(match.group(2), match.group(1)), text)
	text = sub(reLinkUser,
				  lambda match: urlString(match.group(2), "http://www.baka-tsuki.org/project/index.php?title=User:"+match.group(1)),
				  text)
	text = sub(reLinkWikia,
				  lambda match: urlString(match.group(3), "http://%s.wikia.com/wiki/%s" % (match.group(1), match.group(2))),
				  text)

	text = bruteForceReplace(text)

	p = html5lib.HTMLParser(tree=treebuilders.getTreeBuilder("dom"))
	dom_tree = p.parse(text, encoding="utf-8")
	walker = treewalkers.getTreeWalker("dom")
	stream = walker(dom_tree)
#	print "%r" % text.replace("")
#	s = serializer.HTMLSerializer(omit_optional_tags=False,
#								  use_trailing_solidus=True,
#								  space_before_trailing_solidus=False)#strip_whitespace=True
#	output_generator = s.serialize(stream)

#	for item in output_generator:
#		print "%r" % item

	off = 0
	running = True
	isChapterStart = True

	for token in stream:
		#print "%r" % token
		type = token["type"]
		if type == "StartTag":
			name = token["name"]
			if name == "html" or name == "head" or name == "body":
				# ignore...
				pass
			elif name == "reference-placement":
				info.referencePlacement = token["data"][(None, "location")]
			elif name == "heading-style":
				info.headingStyle = token["data"][(None, "style")]
			elif name == "highlight-color":
				info.highlightColor = token["data"][(None, "color")]
			elif name == "references-summary":
				particleList.append(Particle("reference-location"))
			elif name == "info":
				infoType = token["data"][(None, "type")]
				infoValue = token["data"][(None, "value")]
				if infoType == "title":
					info.title = infoValue
				elif infoType == "filename":
					info.filename = infoValue
				elif infoType == "story":
					info.story = infoValue
				elif infoType == "illustrator":
					info.illustrator = infoValue
				elif infoType == "translator":
					info.translator = infoValue
				elif infoType == "editor":
					info.editor = infoValue
				elif infoType == "pdf maker":
					info.pdfMaker = infoValue
				else:
					sys.exit("Unknown <info> %r" % infoType)
			elif name == "local-image":
				name = token["data"][(None, "name")]
				p = Particle("image")
				p.imageName = name
				particleList.append(p)
			elif name == "include-page-template":
				particleList.append(Particle("new-chapter"))

				pageTemplate = token["data"][(None, "title")]
				parsePage(pageTemplate.replace("%27", "'"), cacheDir, reloadPageCache, False) # %27 -> ' still a bit of a hack

				particleList.append(Particle("end-chapter"))
			elif name == "a":
				# FIXME: feels hacky
				href = token["data"][(None, "href")]
				particleList.append(Particle("a href='%s'" % href))

			elif name in ("span", "div", "p"):
				style = token["data"][(None, "style")] if token["data"].get((None, "style")) else ""
				cclass = token["data"][(None, "class")] if token["data"].get((None, "class")) else ""
				p = Particle(name)
				p.style = style
				p.cclass = cclass
				particleList.append(p)

			elif(name == 'new-paragraph' or name == "h1" or name == "h2" or name == "center"
					or name == "h3" or name == "h4" or name == "i" or name == "b"
					or name == "dt" or name == "sup" or name == "nowiki" or name == "ruby" or name == "rt"
					or name == "big" or name == "small" or name == "u" or name == "sub" or name == "del"
					or name == "strike" or name == "s" or name == "strong"):
				if token["data"] != {}:
					print "Token that is supposed to not have data, mysteriously has it: %r" % token
				particleList.append(Particle(name))

			elif(name == "ref"):
				p = Particle(name)
				particleList.append(Particle(name))

			elif(name == "font"):
				particleList.append(Particle(name))

			else:
				print "%r" % token
		elif type == "EmptyTag":
			name = token["name"]
			if name == "hr" or  name == "br":
				particleList.append(Particle(name))
			else:
				print "%r" % token
		elif type == "EndTag":
			name = token["name"]
			if(name == "html" or name == "head" or name == "body" or name == "info"
					or name == "references-summary" or name == "reference-placement"
					or name == "include-page-template" or name == "new-paragraph"
					or name == "local-image" or name == "hr" or name == "heading-style"
					or name == "highlight-color"):
				# ignore...
				pass
			elif(name == "h1" or name == "h2" or name == "ref" or name == "center" or name == "h3" or name == "h4"
					or name == "i" or name == "b" or name == "a" or name == "dt" or name == "sup"
					or name == "nowiki" or name == "ruby" or name == "rt" or name == "span" or name == "div"
					or name == "big" or name == "small" or name == "u" or name == "sub" or name == "del"
					or name == "strike" or name == "s" or name == "p" or name == "strong"):
				particleList.append(Particle("/"+name))
			else:
				print "%r" % token
		elif type == "SpaceCharacters":
			data = token["data"]
			if data == "\n":
				# ignore, single eols don't mean anything in wikiscript
				pass
			elif reSpaces.match(data) != None:
				p = Particle("text")
				p.text = data
				particleList.append(p)
			else:
				sys.exit("Unknown SpaceCharacters %r" % data)
		elif type == "Characters":
			p = Particle("text")
			p.text = token["data"]
			particleList.append(p)
		elif type == "Comment":
			# ignore these for the moment
			pass
		else:
			# spit out an error if we can't identify it...
			print "%r" % token
			# ... but output it as text anyway.
			#p = Particle("text")
			#p.text = "<%s>" % (token["name"][0].upper() + token["name"][1:])
			#particleList.append(p)

	#sys.exit("test")

def loadNovel(templateName, reloadPageCache):
	global particleList
	global cacheDir
	cacheDir = baseCacheDir + templateName + "/"
	del particleList[:]

	if(not os.path.isdir(cacheDir)):
		os.makedirs(cacheDir)

	parsePage(baseTemplateDir + templateName, cacheDir, reloadPageCache, True)
